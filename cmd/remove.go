package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/kkorekk/fdy-cli/services"
)

func init() {
	rootCmd.AddCommand(removeCmd)
}

var removeCmd = &cobra.Command{
	Use:                   "remove",
	Short:                 "Remove the specified plugins",
	Long:                  `Remove packages specified in arguments list`,
	DisableFlagsInUseLine: true,
	Args:                  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c := services.NewCommandService()
		for _, plugin := range args {
			c.Remove(plugin)
		}
	},
}
