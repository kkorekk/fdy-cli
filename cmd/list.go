package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/kkorekk/fdy-cli/services"
)

func init() {
	rootCmd.AddCommand(listCmd)
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List available plugins",
	Long: `List available plugins in form of <plugin-slug> (plugin-label)

If you want to install given plugin, please use <plugin-slug>`,
	DisableFlagsInUseLine: true,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		c := services.NewCommandService()
		c.List()
	},
}
