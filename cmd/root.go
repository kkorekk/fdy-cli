package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/kkorekk/fdy-cli/services"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "fdy-cli",
	Short: "Unofficial command line interface for Fedy",
	Long: fmt.Sprintf(`Unofficial command line interface for Fedy project.

Please visit Fedy's github site for more information: https://github.com/fedy/fedy
This version works with Fedy: %s`, services.FedyVersion),
}

// Execute initialize and execute command line interface
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
