package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/kkorekk/fdy-cli/services"
)

func init() {
	rootCmd.AddCommand(installCmd)
}

var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install the specified plugins",
	Long:  `Install packages specified as an arguments list`,
	DisableFlagsInUseLine: true,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c := services.NewCommandService()
		for _, plugin := range args {
			c.Install(plugin)
		}
	},
}
