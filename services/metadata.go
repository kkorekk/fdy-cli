package services

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

// Exec represent JSON object containing install command
type Exec struct {
	Command string `json:"command"`
}

// Undo represent JSON object containing uninstall command
type Undo struct {
	Command string `json:"command"`
}

// Status represent JSON object containing check command
type Status struct {
	Command string `json:"command"`
}

// Scripts represent JSON object grouping commands for plugin
type Scripts struct {
	Exec   Exec   `json:"exec"`
	Undo   Undo   `json:"undo"`
	Status Status `json:"status"`
}

// Plugin represent Plugin loaded from archive
type Plugin struct {
	Slug        string
	Folder      string
	Label       string  `json:"label"`
	Description string  `json:"description"`
	Category    string  `json:"category"`
	Scripts     Scripts `json:"scripts"`
}

// SlugifyLabel create a slug for a label, slug is used to represent plugin in command line
func (p *Plugin) SlugifyLabel() {
	r := strings.NewReplacer(" ", "-")
	p.Slug = strings.ToLower(r.Replace(p.Label))
}

// PluginNotFoundError returned when plugin cannot be found on the plugins list
type PluginNotFoundError struct {
	msg string
}

func (err PluginNotFoundError) Error() string {
	return err.msg
}

// MetadataService service responsible for loading plugins list from a cache archive
type MetadataService struct {
	archivePath         string
	plugins             []Plugin
	metadataFilePattern string
}

// NewMetadataService initialize a new MetadataService
func NewMetadataService(archivePath string) *MetadataService {
	m := new(MetadataService)
	m.archivePath = archivePath
	m.metadataFilePattern = fmt.Sprintf("fedy-%s/plugins/*/metadata.json", FedyVersion)
	return m
}

// AvailablePlugins return loaded plugins list
func (m *MetadataService) AvailablePlugins() []Plugin {
	return m.plugins
}

// LoadMetadata read Plugins list from a cache archive
func (m *MetadataService) LoadMetadata() error {
	r, err := zip.OpenReader(m.archivePath)
	if err != nil {
		log.Fatalf("Failed to read archive file %s. Reason: %s", m.archivePath, err)
	}
	defer r.Close()

	for _, f := range r.File {
		if match, _ := filepath.Match(m.metadataFilePattern, f.Name); !match {
			continue
		}

		rc, err := f.Open()
		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(rc)
		if err != nil {
			rc.Close()
			return err
		}
		p := Plugin{}

		err = json.Unmarshal(b, &p)
		if err != nil {
			rc.Close()
			return err
		}

		p.SlugifyLabel()
		p.Folder = filepath.Dir(f.Name)
		m.plugins = append(m.plugins, p)
		rc.Close()
	}

	return nil
}

// FindPluginBySlug find given plugin on a plugins list
func (m *MetadataService) FindPluginBySlug(slug string) (Plugin, error) {
	for _, p := range m.plugins {
		if p.Slug == slug {
			return p, nil
		}
	}

	return Plugin{}, PluginNotFoundError{fmt.Sprintf("Plugin %s not found", slug)}
}
