package services

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
)

// FedyVersion version of the Fedy package with which this tool works correctly
const FedyVersion = "4.6.0"

// CacheFilename the name of downloaded Fedy package (ZIP File)
var CacheFilename = fmt.Sprintf("fedycli_cache_%s.zip", FedyVersion)

// DownloadURL url pointing to the Fedy release file
var DownloadURL = fmt.Sprintf("https://github.com/fedy/fedy/archive/v%s.zip", FedyVersion)

// DownloadService service responsible for downloading the Fedy package
type DownloadService struct {
	FilePath string
}

// NewDownloadService initialize a new DownloadService
func NewDownloadService(cacheFolder string) *DownloadService {
	f := path.Join(cacheFolder, CacheFilename)
	return &DownloadService{f}
}

// EnsureArchiveExist check if package exists in cache location if not download it
func (c *DownloadService) EnsureArchiveExist() {
	cacheExist, err := c.checkCacheExist()

	if err != nil {
		fmt.Printf("Error: Failed to check ZIP file, %s\n", err)
		os.Exit(1)
	}

	if cacheExist {
		return
	}

	fmt.Printf("Downloading Fedy ZIP file from %s\n", DownloadURL)

	err = c.downloadArchive()
	if err != nil {
		fmt.Printf("Error: Failed to download ZIP file, %s", err)
	}
}

func (c *DownloadService) checkCacheExist() (bool, error) {
	_, err := os.Stat(c.FilePath)

	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (c *DownloadService) downloadArchive() error {
	err := os.MkdirAll(path.Dir(c.FilePath), os.ModePerm)
	if err != nil {
		return err
	}

	out, err := os.Create(c.FilePath)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(DownloadURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
