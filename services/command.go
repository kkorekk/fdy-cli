package services

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

// CommandService use other services to provide functionality required by cli tools
type CommandService struct {
	downloadService *DownloadService
	metadataService *MetadataService
}

// NewCommandService initialize a new CommandService
func NewCommandService() *CommandService {
	c := new(CommandService)
	c.downloadService = NewDownloadService("/var/cache")
	c.downloadService.EnsureArchiveExist()
	c.metadataService = NewMetadataService(c.downloadService.FilePath)
	c.metadataService.LoadMetadata()
	return c
}

// List print available plugins to stdout
func (c *CommandService) List() {
	plugins := c.metadataService.AvailablePlugins()
	for _, p := range plugins {
		fmt.Printf("%s (%s)\n", p.Slug, p.Label)
	}
}

// Install install given plugin
func (c *CommandService) Install(pluginSlug string) {
	plugin, err := c.metadataService.FindPluginBySlug(pluginSlug)
	if err != nil {
		fmt.Printf("Error: %s not found\n", pluginSlug)
		os.Exit(1)
	}

	dir, err := c.extractPluginFromArchive(plugin.Folder)
	if err != nil {
		fmt.Printf("Error: Failed to extract plugin files from archive, %s\n", err)
		os.Exit(1)
	}
	defer os.RemoveAll(dir)

	err = c.callSubcommand(plugin.Scripts.Status.Command, dir)
	if err == nil {
		fmt.Println("Error: Plugin already installed")
		os.Exit(1)
	}

	err = c.callSubcommand(plugin.Scripts.Exec.Command, dir)
	if err != nil {
		fmt.Println("Error: Installation failed")
		os.Exit(1)
	}
}

// Remove remove given plugin
func (c *CommandService) Remove(pluginSlug string) {
	plugin, err := c.metadataService.FindPluginBySlug(pluginSlug)
	if err != nil {
		fmt.Printf("Error: %s not found\n", pluginSlug)
		os.Exit(1)
	}

	dir, err := c.extractPluginFromArchive(plugin.Folder)
	if err != nil {
		fmt.Printf("Error: Failed to extract plugin files from archive, %s\n", err)
		os.Exit(1)
	}
	defer os.RemoveAll(dir)

	err = c.callSubcommand(plugin.Scripts.Status.Command, dir)
	if err != nil {
		fmt.Println("Error: Plugin not installed")
		os.Exit(1)
	}

	err = c.callSubcommand(plugin.Scripts.Undo.Command, dir)
	if err != nil {
		fmt.Println("Error: Uninstallation failed")
		os.Exit(1)
	}
}

func (c *CommandService) extractPluginFromArchive(pluginPath string) (string, error) {
	dir, err := ioutil.TempDir("/tmp", "fedy_cli_")
	if err != nil {
		return "", err
	}
	pluginFolderGlob := path.Join(pluginPath, "*.*")

	r, err := zip.OpenReader(c.downloadService.FilePath)
	defer r.Close()

	for _, f := range r.File {
		if match, _ := filepath.Match(pluginFolderGlob, f.Name); !match {
			continue
		}

		rc, err := f.Open()
		if err != nil {
			return "", err
		}

		t, err := os.Create(path.Join(dir, path.Base(f.Name)))
		if err != nil {
			rc.Close()
			return "", err
		}

		_, err = io.Copy(t, rc)
		if err != nil {
			rc.Close()
			t.Close()
			return "", err
		}
		rc.Close()
		t.Close()
	}

	return dir, nil
}

func (c *CommandService) callSubcommand(command string, workDir string) error {
	var cmd *exec.Cmd

	s := strings.Split(command, " ")
	if s[0] == "run-as-root" {
		s = s[1:]
	}

	if s[0] == "-s" || s[0] == "--script" {
		s = s[1:]
		cmd = exec.Command("sh", s...)
	} else {
		name := s[0]
		var args []string
		if len(s) > 1 {
			args = s[1:]
		}
		cmd = exec.Command(name, args...)
	}

	cmd.Dir = workDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
