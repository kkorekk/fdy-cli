# fdy-cli

This repository contains `fdy-cli` package which is an unofficial command line tool for Fedy project. I created this project
as a part of my golang learning process.

## How to use this tool

Current version `0.0.1` support three basic operations.

### Listing packages

`fdy-cli list`

### Installing packages

`fdy-cli install arc-icon-theme`

### Uninstalling package

`fdy-cli uninstall arc-icon-theme`
